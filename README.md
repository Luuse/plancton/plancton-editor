# PLANCTON
Plancton is a [metapost](https://en.wikipedia.org/wiki/MetaPost) font editor.
(This is a dev version.)

## Requiquements
 * [Fontforge](http://fontforge.github.io) 
 * [Inkscape](https://inkscape.org/)
 * [TexLive](https://tug.org/texlive/)
 * Python2 or Python3
 * Virtualen

## Install

 * 1 `git clone https://gitlab.com/Luuse/Luuse.tools/plancton-editor.git`
 * 2 `cd plancton-editor`
 * 3 `virtualenv plancton-env`
 * 4 `source plancton-env/bin/activate`
 * 5 `pip install -r requirements.txt`

## Start server

`python plancton-server.py` listening on `http://localhost:8080/`

## Contributors
* [Luuse](http://www.luuse.io/)

## License

GNU General Public License - [gpl3](https://www.gnu.org/licenses/gpl-3.0.en.html


